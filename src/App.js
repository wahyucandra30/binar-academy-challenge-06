import './App.css';
import React from 'react';
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import "./styles/tailwind.css"
import { useEffect } from 'react';
import { auth } from './firebase/firebase-config';
import { useDispatch } from 'react-redux';


function App() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  useEffect(() => {
    document.title = "Home";
    // const storageItem = JSON.parse(localStorage.getItem("persist:root"))
    // const token = storageItem.loginReducer.userData?.access_Token ? storageItem.loginReducer.userData.access_Token : "";
    // console.log("TOKEN: " + token);
    // if (token === "" && location.pathname !== "/register") {
    //   navigate("/login");
    // }
    auth.onAuthStateChanged(user => {
      if (user) {
        console.log(user)
        dispatch({ type: "SET_USER_DATA", payload: user.userData }) 
        navigate(location)
      } 
      else if(location.pathname !== "/register"){
        navigate("/login")
      }
    })
  }, [])

  return (
    <>
      <Outlet />
    </>
  );
}

export default App;
