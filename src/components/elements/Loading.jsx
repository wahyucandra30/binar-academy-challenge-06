const Loading = () => {
    return (
        <div className="flex justify-center w-full col-span-3">
            <img src="/icon_spinner.svg" alt="Loading" className="animate-spin w-10 h-10" />
        </div>
    )
}
export default Loading;