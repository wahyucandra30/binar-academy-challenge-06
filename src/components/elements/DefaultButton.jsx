import { Link } from "react-router-dom";

const DefaultButton = ({ placeholder, linkTo, action, iconURL,
    bgColor, bgHoverColor, defaultWidth, defaultHeight, mdWidth, mdHeight,
    textColor, textHoverColor, extraStyles }) => {
    const color = bgColor ? bgColor : "bg-limegreen-4";
    const hoverColor = bgHoverColor ? bgHoverColor : "hover:bg-limegreen-5";
    const width = defaultWidth ? defaultWidth : "w-fit";
    const height = defaultHeight ? defaultHeight : "h-fit"
    return (
        <Link to={linkTo ? linkTo : ""} onClick={action ? action : ""}>
            <button type="button" className={`font-bold rounded-sm text-sm text-center items-center 
            ${color} ${hoverColor} ${width} ${height} ${mdWidth ? mdWidth : ""} ${mdHeight ? mdHeight : ""} ${extraStyles ? extraStyles : ""} 
            ${textColor ? textColor : "text-white"} ${textHoverColor ? textHoverColor : "hover:text-white"} `}>
                {placeholder ? placeholder : "Button"}
            </button>
        </Link>
    )
}

export default DefaultButton;