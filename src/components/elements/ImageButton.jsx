import { Link } from "react-router-dom";

const ImageButton = ({ placeholder, linkTo, action, iconURL, iconWidth, iconHeight, iconBgColor,
    bgColor, bgHoverColor, defaultWidth, defaultHeight, mdWidth, mdHeight,
    textColor, textHoverColor, extraStyles }) => {
    const icon = iconURL ? iconURL : "";
    const color = bgColor ? bgColor : "bg-limegreen-4";
    const hoverColor = bgHoverColor ? bgHoverColor : "hover:bg-limegreen-5";
    const width = defaultWidth ? defaultWidth : "w-fit";
    const height = defaultHeight ? defaultHeight : "h-fit"
    return (
        <Link to={linkTo ? linkTo : ""} onClick={action ? action : ""}>
            <button type="button" className={`rounded-sm text-sm text-center items-center flex justify-evenly
            ${color} ${hoverColor} ${width} ${height} ${mdWidth ? mdWidth : ""} ${mdHeight ? mdHeight : ""} ${extraStyles ? extraStyles : ""} 
            ${textColor ? textColor : "text-white"} ${textHoverColor ? textHoverColor : "hover:text-white"} `}>
                <div className={`h-full w-14 flex justify-center items-center ${iconBgColor ? iconBgColor : "bg-white"}`}>
                    <img src={icon} alt="icon" width={iconWidth} height={iconHeight} />
                </div>
                <div className="w-full">
                    {placeholder ? placeholder : "Button"}
                </div>
                <div className="w-5">

                </div>
            </button>
        </Link>
    )
}

export default ImageButton;