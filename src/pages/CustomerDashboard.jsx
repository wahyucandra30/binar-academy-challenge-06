import Navbar from '../components/Navbar'
import Footer from "../components/Footer";
import Welcome from '../components/Welcome';
import Search from '../components/Search';
import { Outlet } from 'react-router-dom';
const CustomerDashboard = () => {
    return <>
        <Navbar />
        <Welcome />
        <Search />
        <Outlet />
        <Footer />
    </>
}

export default CustomerDashboard;