import { useState } from "react"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom";
import { registerWithEmailandPassword, loginWithGoogle, loginWithFacebook } from "../store/actions/auth";
import DefaultButton from "../components/elements/DefaultButton"
import ImageButton from "../components/elements/ImageButton"

const Register = () => {
    const [showAlert, setShowAlert] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const dispatch = useDispatch();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const handleEmailInput = (e) => {
        setEmail(e.target.value);
    }
    const handlePasswordInput = (e) => {
        setPassword(e.target.value);
    }
    return <>
        <div className="bg-login-image bg-darkblue-4 w-screen h-screen flex justify-end">
            <div className="w-[482px] bg-white h-full flex flex-col justify-center px-14 gap-8">
                <div className="bg-darkblue-1 w-[100px] h-[34px]"></div>
                <h1 className="text-2xl font-bold">Create New Account</h1>
                {showAlert &&
                    <div className="w-full h-auto text-[#D00C1A] font-light bg-[#D00C1A] bg-opacity-10 pl-6 pr-4 py-3 rounded-[5px]">
                        Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.
                    </div>
                }
                {showSuccess &&
                    <div className="w-full h-auto text-[#23d00c] bg-[#23d00c] bg-opacity-10 pl-6 pr-4 py-3 rounded-[5px]">
                        Akun berhasil terdaftar
                    </div>
                }
                <div>
                    <h6 className="text-sm mb-2">Email</h6>
                    <input value={email} onChange={(e) => handleEmailInput(e)} type="text" placeholder="Contoh: johndee@gmail.com"
                        // ref={(el) => this.emailField = el}
                        className="h-[38px] w-full p-4 border-[1px] rounded-md mb-4" />
                    <h6 className="text-sm mb-2">Password</h6>
                    <input value={password} onChange={(e) => handlePasswordInput(e)} type="password" placeholder="6+ Karakter"
                        // ref={(el) => this.passwordField = el}
                        className="h-[38px] w-full p-4 border-[1px] rounded-md" />
                </div>
                <div>
                    <DefaultButton
                        action={() => registerWithEmailandPassword(email, password, setShowAlert, setShowSuccess)}
                        bgColor={"bg-darkblue-4"} bgHoverColor={"hover:bg-darkblue-5"}
                        defaultWidth={"w-full"} defaultHeight={"h-9"}
                        placeholder={"Sign Up"}
                        extraStyles={"rounded-sm"} />
                    <h5 className="text-xs mt-2">
                        {"Already have an account? "}
                        <a href="/login" className="text-blue-500 font-semibold hover:underline">Sign in</a>
                    </h5>
                    <div class="relative flex py-3 items-center">
                        <div class="flex-grow border-t border-gray-400"></div>
                        <span class="flex-shrink mx-4 text-gray-400">OR</span>
                        <div class="flex-grow border-t border-gray-400"></div>
                    </div>
                    <div className="flex flex-col gap-4">
                        <ImageButton action={loginWithGoogle(dispatch, navigate)}
                            bgColor={"bg-neutral-1"} bgHoverColor={"hover:bg-blue-400"}
                            iconURL={"/icon_google.png"} iconWidth="24px" iconHeight={"32px"}
                            textColor={"text-gray-500"} textHoverColor={"hover:text-white"}
                            defaultWidth={"w-full"} defaultHeight={"h-11"}
                            placeholder={"Sign Up with Google"}
                            extraStyles={"border-2 hover:border-blue-400 rounded-sm font-semibold "} />
                        <ImageButton action={loginWithFacebook(dispatch, navigate)}
                            bgColor={"bg-[#3b5998]"} bgHoverColor={"hover:bg-[#2b447b]"}
                            iconURL={"/icon_facebook.png"} iconWidth="12px" iconHeight={"12px"}
                            iconBgColor={"bg-transparent"}
                            textColor={"text-white"} textHoverColor={"hover:text-white"}
                            defaultWidth={"w-full"} defaultHeight={"h-11"}
                            placeholder={"Sign Up with Facebook"}
                            extraStyles={"rounded-sm font-semibold "} />
                    </div>

                </div>
            </div>
        </div>
    </>
}

export default Register;