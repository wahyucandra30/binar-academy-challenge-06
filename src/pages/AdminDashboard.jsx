import { useNavigate } from "react-router-dom";
import {logOut} from "../store/actions/auth"
import { useDispatch } from "react-redux";
const AdminDashboard = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    return <>
        <div className="w-screen h-screen flex flex-row justify-center">
            <div className="min-w-[70px] h-full bg-darkblue-4 flex flex-col items-center">
                <div className="w-full h-[70px] flex justify-center items-center">
                    <div className="w-[34px] h-[34px] bg-darkblue-0"></div>
                </div>
                <button className="w-full h-16 hover:bg-darkblue-2 flex flex-col items-center justify-center gap-1 hover:font-bold text-center">
                    <img src="/icon_home.svg" alt="" />
                    <h1 className="text-white">Dashboard</h1>
                </button>
                <button className="w-full h-16 hover:bg-darkblue-2 flex flex-col items-center justify-center gap-1 hover:font-bold text-center">
                    <img src="/icon_truck.svg" alt="" />
                    <h1 className="text-white">Cars</h1>
                </button>
            </div>

            <div className="w-full h-full flex flex-col">
                <div className="w-full bg-white min-h-[70px] shadow z-10 flex items-center px-4 justify-between">
                    <div className="bg-darkblue-1 w-[100px] h-[34px]"></div>
                    <div className="w-auto flex justify-between gap-7 items-center">
                        <div className="flex items-center">
                            <img src="/icon_search.svg" alt="" className="absolute ml-3" />
                            <input type="text" className="border rounded-sm pl-9 w-[174px] h-[34px]" placeholder="Search" />
                            <button className="border border-darkblue-4 rounded-sm px-4 h-[34px] bg-white hover:bg-darkblue-4
                        text-darkblue-4 hover:text-white font-bold">Search</button>
                        </div>
                        <div className="flex items-center gap-2">
                            <div className="rounded-full bg-darkblue-1 w-[38px] h-[38px] flex items-center justify-center
                        text-darkblue-4 font-bold text-sm">
                                U
                            </div>
                            <h1 className="font-bold">Umar Bakri</h1>
                            <img src="/icon_downarrow.svg" alt="" />
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
                <div className="flex w-full h-full">
                    <div className="min-w-[220px] bg-white h-full shadow-sm py-5">
                        <h1 className="font-bold text-sm text-neutral-3 mx-4 mb-4">DASHBOARD</h1>
                        <button className="w-full bg-darkblue-1 px-4 py-2 flex justify-start">
                            <h2 className="font-bold text-sm text-black">Dashboard</h2>
                        </button>
                    </div>
                    <div className="w-full h-full bg-neutral-100 px-5 py-14">
                        <h1 className="text-3xl font-bold mb-2">Welcome, Admin BCR</h1>
                        <button onClick={() => logOut(dispatch, navigate)}
                        className="bg-red-600 hover:bg-red-700 px-5 py-2 rounded-sm text-white font-bold">Sign out</button>
                    </div>
                </div>
            </div>
        </div>
    </>
}
export default AdminDashboard;